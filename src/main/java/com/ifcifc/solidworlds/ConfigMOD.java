package com.ifcifc.solidworlds;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class ConfigMOD {

    private static Gson gson;
    public static File fileJSON;
    public static final int version = 2;

    public static config options = new config(version);
    public static config defaultOptions = new config(version);

    public static String worldName = "none";

    public static void initialize() {
        fileJSON = new File(FabricLoader.getInstance().getConfigDirectory(), "solidworlds.json");

        gson = new GsonBuilder().setPrettyPrinting().create();

        defaultOptions = loadConf(fileJSON);
        options = defaultOptions;
        updateSave(fileJSON, defaultOptions);
    }

    public static config loadConf(File conf) {
        if (conf.exists()) {
            config cfg;
            try {
                cfg = gson.fromJson(new FileReader(conf), config.class);
                if (cfg.version == version) {
                    System.out.println("SolidWorlds: Load Config");
                    return cfg;
                }
                System.out.println("SolidWorlds: Update Old Config");
                return convert(cfg);
            } catch (Exception e) {
                System.out.println("SolidWorlds: Error to read config file");
            }

        } else System.out.println("SolidWorlds: Create Config");

        return createConfig(conf, false);
    }

    public static void loadWorldConfig() {
        File conf = new File(FabricLoader.getInstance().getGameDirectory(), "/saves/" + ConfigMOD.worldName + "/solidWordls.json");
        if (!conf.exists()) ConfigMOD.createConfig(conf, true);
        options = loadConf(conf);
    }

    public static config createConfig(File conf, boolean fromDefault) {
        config ret = (fromDefault) ? ConfigMOD.defaultOptions : new config(ConfigMOD.version);
        ConfigMOD.updateSave(conf, ret);
        return ret;
    }

    public static void updateSave(File conf, config options) {
        try {
            final FileWriter sFile = new FileWriter(conf);
            sFile.write(gson.toJson(options));
            sFile.close();
        } catch (Exception e) {
            System.out.println("SolidWorlds: ERROR SAVE CONFIG");
        }
    }

    public static config convert(config cfg) {
        config ret = new config(ConfigMOD.version);
        switch (cfg.version) {
            case 1:
                ret.Cave = cfg.Cave;
                ret.Ravine = cfg.Ravine;
                ret.UnderwaterCave = cfg.UnderwaterCave;
                ret.UnderwaterRavine = cfg.UnderwaterRavine;
                ret.Mineshaft = cfg.Mineshaft;
                ret.WaterLake = cfg.WaterLake;
                ret.LavaLake = cfg.LavaLake;
                //case 2:
                // case 3:
        }

        return ret;
    }

    public static class config {
        public final int version;
        public boolean Cave = false;
        public boolean Ravine = false;
        public boolean UnderwaterCave = false;
        public boolean UnderwaterRavine = false;
        public boolean Mineshaft = true;
        public boolean WaterLake = false;
        public boolean LavaLake = false;

        public boolean SurfaceLavaLake = false;
        public boolean SurfaceWaterLake = false;
        public int SurfaceLavaLakeMinY = 60;
        public int SurfaceWaterLakeMinY = 60;
        public int SurfaceLavaLakeHeight = 32;
        public int SurfaceWaterLakeHeight = 32;
        public int SurfaceLavaLakeChance = 45;
        public int SurfaceWaterLakeChance = 45;

        public config(int version, boolean cave, boolean ravine, boolean underwaterCave, boolean underwaterRavine, boolean mineshaft, boolean waterLake, boolean lavaLake, boolean surfaceLavaLake, boolean surfaceWaterLake, int surfaceLavaLakeMinY, int surfaceWaterLakeMinY, int surfaceLavaLakeHeight, int surfaceWaterLakeHeight, int surfaceLavaLakeChance, int surfaceWaterLakeChance) {
            this.version = version;
            Cave = cave;
            Ravine = ravine;
            UnderwaterCave = underwaterCave;
            UnderwaterRavine = underwaterRavine;
            Mineshaft = mineshaft;
            WaterLake = waterLake;
            LavaLake = lavaLake;
            SurfaceLavaLake = surfaceLavaLake;
            SurfaceWaterLake = surfaceWaterLake;
            SurfaceLavaLakeMinY = surfaceLavaLakeMinY;
            SurfaceWaterLakeMinY = surfaceWaterLakeMinY;
            SurfaceLavaLakeHeight = surfaceLavaLakeHeight;
            SurfaceWaterLakeHeight = surfaceWaterLakeHeight;
            SurfaceLavaLakeChance = surfaceLavaLakeChance;
            SurfaceWaterLakeChance = surfaceWaterLakeChance;
        }

        public config(int version) {
            this.version = version;
        }


    }

    public static String getLevelName() {
        try {

            return (MinecraftClient.getInstance().isIntegratedServerRunning()) ?
                    MinecraftClient.getInstance().getServer().getIconFile().getParentFile().getName() :
                    "default";
        } catch (Exception e) {
            return "default";
        }
    }
}
