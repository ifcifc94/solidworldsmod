package com.ifcifc.solidworlds.mixin;


import com.ifcifc.solidworlds.ConfigMOD;
import com.ifcifc.solidworlds.SurfaceLakes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.decorator.ChanceDecoratorConfig;
import net.minecraft.world.gen.decorator.DecoratorContext;
import net.minecraft.world.gen.decorator.WaterLakeDecorator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Random;
import java.util.stream.Stream;

@Mixin(WaterLakeDecorator.class)
public class WaterLakeDecoratorMixin {
    @Inject(method = "getPositions",
            cancellable = true,
            at = @At("HEAD"))
    public void getPositions(DecoratorContext decoratorContext, Random random, ChanceDecoratorConfig chanceDecoratorConfig, BlockPos blockPos, CallbackInfoReturnable<Stream<BlockPos>> cir) {
        if (ConfigMOD.options.SurfaceWaterLake) {
            cir.setReturnValue(SurfaceLakes.generateLake(
                    random,
                    blockPos,
                    ConfigMOD.options.SurfaceWaterLakeChance,
                    ConfigMOD.options.SurfaceWaterLakeMinY,
                    ConfigMOD.options.SurfaceWaterLakeHeight
            ));
        } else if (!ConfigMOD.options.WaterLake) cir.setReturnValue(Stream.empty());
    }
}
