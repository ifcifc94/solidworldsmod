package com.ifcifc.solidworlds.mixin;


import com.ifcifc.solidworlds.ConfigMOD;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.ProbabilityConfig;
import net.minecraft.world.gen.carver.CaveCarver;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.BitSet;
import java.util.Random;
import java.util.function.Function;

@Mixin(CaveCarver.class)
public class CaveCarverMixin {
    @Inject(method = "carve",
            cancellable = true,
            at = @At("HEAD"))
    public void carve(Chunk chunk, Function<BlockPos, Biome> function, Random random, int i, int j, int k, int l, int m, BitSet bitSet, ProbabilityConfig probabilityConfig, CallbackInfoReturnable<Boolean> cir) {
        if (!ConfigMOD.options.Cave) cir.setReturnValue(false);
    }
}
