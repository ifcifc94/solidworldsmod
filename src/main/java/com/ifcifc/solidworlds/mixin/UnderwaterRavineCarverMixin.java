package com.ifcifc.solidworlds.mixin;


import com.ifcifc.solidworlds.ConfigMOD;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.carver.UnderwaterRavineCarver;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.BitSet;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

@Mixin(UnderwaterRavineCarver.class)
public class UnderwaterRavineCarverMixin {
    @Inject(method = "carveAtPoint",
            cancellable = true,
            at = @At("HEAD"))
    protected void carveAtPoint(Chunk chunk, Function<BlockPos, Biome> posToBiome, BitSet carvingMask, Random random, BlockPos.Mutable mutable, BlockPos.Mutable mutable2, BlockPos.Mutable mutable3, int seaLevel, int mainChunkX, int mainChunkZ, int x, int z, int relativeX, int y, int relativeZ, MutableBoolean mutableBoolean, CallbackInfoReturnable<Boolean> cir) {
        if (!ConfigMOD.options.UnderwaterRavine) cir.setReturnValue(false);
    }
}
