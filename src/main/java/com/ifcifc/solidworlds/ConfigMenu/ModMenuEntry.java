package com.ifcifc.solidworlds.ConfigMenu;

import com.ifcifc.solidworlds.ConfigMOD;
import io.github.prospector.modmenu.api.ConfigScreenFactory;
import io.github.prospector.modmenu.api.ModMenuApi;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;

import java.io.File;

public class ModMenuEntry implements ModMenuApi {
    @Override
    public String getModId() {
        return "solidworlds";
    }

    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return this::getScreen;
    }


    public Screen getScreen(Screen parent) {
        ConfigMOD.config Cfg = (MinecraftClient.getInstance().world == null) ? ConfigMOD.defaultOptions : ConfigMOD.options;
        return SettingMenu.getConfigBuilderScreen(Cfg).setSavingRunnable(() -> {
            if (MinecraftClient.getInstance().world == null) {
                ConfigMOD.updateSave(ConfigMOD.fileJSON, Cfg);
                ConfigMOD.initialize();
            } else {
                ConfigMOD.updateSave(
                        new File(FabricLoader.getInstance().getGameDirectory(), "/saves/" + ConfigMOD.worldName + "/solidWordls.json"),
                        Cfg
                );
                ConfigMOD.loadWorldConfig();
            }
        }).setParentScreen(parent).build();
    }


}