package com.ifcifc.solidworlds;

import net.minecraft.util.math.BlockPos;

import java.util.Random;
import java.util.stream.Stream;

public class SurfaceLakes {

    public static Stream<BlockPos> generateLake(Random random, BlockPos blockPos, int chance, int minY, int maxY) {
        if (random.nextInt(chance) == 0) {
            return Stream.of(new BlockPos(
                    random.nextInt(16) + blockPos.getX(),
                    random.nextInt(maxY) + minY,
                    random.nextInt(16) + blockPos.getZ()));
        } else {
            return Stream.empty();
        }
    }
}
